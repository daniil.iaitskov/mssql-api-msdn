<!DOCTYPE html>

















































<html class="hasSidebar hasPageActions hasBreadcrumb conceptual has-default-focus theme-light" lang="en-us" dir="ltr" data-css-variable-support="true" data-authenticated="false" data-auth-status-determined="false" data-target="docs" x-ms-format-detection="none">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta property="og:title" content="KEY_ID (Transact-SQL) - SQL Server" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://learn.microsoft.com/en-us/sql/t-sql/functions/key-id-transact-sql" />
			<meta property="og:description" content="KEY_ID (Transact-SQL)" />

	<meta property="og:image" content="https://learn.microsoft.com/en-us/media/logos/logo-ms-social.png" />

	<meta property="og:image:alt" content="Microsoft Logo" />

	<meta name="twitter:card" content="summary" />

	<meta name="twitter:site" content="@docsmsft" />

	<meta name="color-scheme" content="light dark">


	<meta name="author" content="VanMSFT" />
<meta name="breadcrumb_path" content="../../breadcrumb/toc.json" />
<meta name="depot_name" content="SQL.sql-content" />
<meta name="description" content="KEY_ID (Transact-SQL)" />
<meta name="document_id" content="139f9d67-ee53-2efb-3491-a37b3c8e4f89" />
<meta name="document_version_independent_id" content="ad1a3a90-29e9-9115-dd06-02120b7ff0d6" />
<meta name="gitcommit" content="https://github.com/MicrosoftDocs/sql-docs-pr/blob/b5a90dc0747e4fe8d3057a6e3fad993c8505d01d/docs/t-sql/functions/key-id-transact-sql.md" />
<meta name="locale" content="en-us" />
<meta name="manager" content="jroth" />
<meta name="monikerRange" content="= azuresqldb-current || = azuresqldb-mi-current || >= sql-server-2016 || >= sql-server-linux-2017 || = azuresqledge-current" />
<meta name="monikers" content="azuresqldb-current" />
<meta name="monikers" content="azuresqldb-mi-current" />
<meta name="monikers" content="sql-server-2016" />
<meta name="monikers" content="sql-server-2017" />
<meta name="monikers" content="sql-server-linux-2017" />
<meta name="monikers" content="sql-server-linux-ver15" />
<meta name="monikers" content="sql-server-linux-ver16" />
<meta name="monikers" content="sql-server-ver15" />
<meta name="monikers" content="sql-server-ver16" />
<meta name="ms.author" content="vanto" />
<meta name="ms.date" content="03/06/2017" />
<meta name="ms.service" content="sql" />
<meta name="ms.subservice" content="t-sql" />
<meta name="ms.topic" content="reference" />
<meta name="original_content_git_url" content="https://github.com/MicrosoftDocs/sql-docs-pr/blob/live/docs/t-sql/functions/key-id-transact-sql.md" />
<meta name="page_type" content="conceptual" />
<meta name="pdf_url_template" content="https://learn.microsoft.com/pdfstore/en-us/SQL.sql-content/{branchName}{pdfName}" />
<meta name="recommendations" content="true" />
<meta name="schema" content="Conceptual" />
<meta name="search.mshattr.devlang" content="tsql" />
<meta name="site_name" content="Docs" />
<meta name="toc_preview" content="true" />
<meta name="toc_rel" content="../../toc.json" />
<meta name="uhfHeaderId" content="MSDocsHeader-DocsSQL" />
<meta name="updated_at" content="2022-11-18 11:19 PM" />
<meta name="word_count" content="192" />




	<meta name="cmProducts" content="https://authoring-docs-microsoft.poolparty.biz/devrel/cbe4ca68-43ac-4375-aba5-5945a6394c20" data-source="generated" />

	<meta name="cmProducts" content="https://authoring-docs-microsoft.poolparty.biz/devrel/540ac133-a371-4dbb-8f94-28d6cc77a70b" data-source="generated" />

	<meta name="cmProducts" content="https://authoring-docs-microsoft.poolparty.biz/devrel/6ab7faaf-d791-4a26-96a2-3b11738538e7" data-source="generated" />



	<meta name="scope" content="sql" />
	<meta name="github_feedback_content_git_url" content="https://github.com/MicrosoftDocs/sql-docs/blob/live/docs/t-sql/functions/key-id-transact-sql.md" />
<link href="https://learn.microsoft.com/en-us/sql/t-sql/functions/key-id-transact-sql" rel="canonical">
	<title>KEY_ID (Transact-SQL) - SQL Server | Microsoft Learn</title>

		<link rel="stylesheet" href="/_themes/docs.theme/master/en-us/_themes/styles/a18eeeea.site-ltr.css ">



	<script id="msdocs-script">
	var msDocs = {
		data: {
			timeOrigin: Date.now(),
			contentLocale: 'en-us',
			contentDir: 'ltr',
			userLocale: 'en-us',
			userDir: 'ltr',
			pageTemplate: 'Conceptual',
			brand: '',
			context: {
			},
			hasBinaryRating: true,
			hasGithubIssues: false,
			standardFeedback: false,
			showFeedbackReport: false,
			enableTutorialFeedback: false,
			feedbackSystem: 'None',
			feedbackGitHubRepo: '',
			feedbackProductUrl: '',
			extendBreadcrumb: true,
			isEditDisplayable: true,
			hideViewSource: false,
			hasPageActions: true,
			hasPrintButton: true,
			hasBookmark: true,
			hasShare: true,
			hasRecommendations: true,
			contributors: [
						{ name: "VanMSFT", url: "https://github.com/VanMSFT" },
						{ name: "rothja", url: "https://github.com/rothja" },
						{ name: "niko-neugebauer", url: "https://github.com/niko-neugebauer" },
						{ name: "cawrites", url: "https://github.com/cawrites" },
						{ name: "icoric", url: "https://github.com/icoric" },
						{ name: "markingmyname", url: "https://github.com/markingmyname" },
						{ name: "MikeRayMSFT", url: "https://github.com/MikeRayMSFT" },
						{ name: "craigg-msft", url: "https://github.com/craigg-msft" },
						{ name: "edmacauley", url: "https://github.com/edmacauley" }
],
		},
		functions:{}
	};
	</script>
	<script src="https://wcpstatic.microsoft.com/mscc/lib/v2/wcp-consent.js"></script>
	<script src="https://js.monitor.azure.com/scripts/c/ms.jsll-3.min.js"></script>

	<script src="/_themes/docs.theme/master/en-us/_themes/global/67a45209.deprecation.js"></script>
		<script src="/_themes/docs.theme/master/en-us/_themes/scripts/14a90406.index-docs.js"></script>
</head>

<body lang="en-us" dir="ltr">
	<div class="header-holder has-default-focus">
		<a href="#main" class="skip-to-main-link has-outline-color-text visually-hidden-until-focused position-fixed has-inner-focus focus-visible top-0 left-0 right-0 padding-xs has-text-centered has-body-background" tabindex="1">Skip to main content</a>

		<div hidden id="cookie-consent-holder"></div>

		<div id="unsupported-browser" style="
			background-color: white;
			color: black;
			padding: 16px;
			border-bottom: 1px solid grey;"
			hidden
		>
			<div style="max-width: 800px; margin: 0 auto;">
				<p style="font-size: 24px">This browser is no longer supported.</p>
				<p style="font-size: 16px; margin-top: 16px;">Upgrade to Microsoft Edge to take advantage of the latest features, security updates, and technical support.</p>
				<div style="margin-top: 12px;">
					<a href="https://go.microsoft.com/fwlink/p/?LinkID=2092881 "
						style="
						background-color: #0078d4;
						border: 1px solid #0078d4;
						color: white;
						padding: 6px 12px;
						border-radius: 2px;
						display: inline-block;
						">
Download Microsoft Edge					</a>
					<a href="https://learn.microsoft.com/en-us/lifecycle/faq/internet-explorer-microsoft-edge"
						style="
							background-color: white;
							padding: 6px 12px;
							border: 1px solid #505050;
							color: #171717;
							border-radius: 2px;
							display: inline-block;
							">
More info about Internet Explorer and Microsoft Edge					</a>
				</div>
			</div>

		</div>
		<!-- liquid-tag banners global -->
		<div id="headerAreaHolder" data-bi-name="header">
<header role="banner" itemscope="itemscope" itemtype="http://schema.org/Organization">
	<div class="nav-bar">
		<div class="nav-bar-brand">
			<a itemprop="url" href="https://www.microsoft.com" aria-label="Microsoft" class="nav-bar-button">
				<div class="nav-bar-logo has-background-image theme-display is-light" role="presentation" aria-hidden="true" itemprop="logo" itemscope="itemscope"></div>
				<div class="nav-bar-logo has-background-image theme-display is-dark is-high-contrast" role="presentation" aria-hidden="true" itemprop="logo" itemscope="itemscope"></div>
			</a>
		</div>
	</div>
	<div class="nav-bar border-top is-hidden-mobile"></div>
</header>		</div>


			<div id="content-header" class="content-header uhf-container has-padding has-default-focus border-bottom-none" data-bi-name="content-header">
				<div class="content-header-controls margin-xxs margin-inline-sm-tablet">
					<button type="button" class="contents-button button button-sm margin-right-xxs" data-bi-name="contents-expand" aria-haspopup="true" data-contents-button>
						<span class="icon"><span class="docon docon-menu" aria-hidden="true"></span></span>
						<span class="contents-expand-title">
Table of contents						</span>
					</button>
					<button type="button" class="ap-collapse-behavior ap-expanded button button-sm" data-bi-name="ap-collapse" aria-controls="action-panel">
						<span class="icon"><span class="docon docon-exit-mode" aria-hidden="true"></span></span>
						<span>Exit focus mode</span>
					</button>
				</div>
			</div>

		<div id="disclaimer-holder" class="has-overflow-hidden has-default-focus">
			<!-- liquid-tag banners sectional -->
		</div>
	</div>

	<div class="mainContainer  uhf-container has-default-focus" data-bi-name="body">

		<div class="columns has-large-gaps is-gapless-mobile ">

			<div id="left-container" class="left-container is-hidden-mobile column is-one-third-tablet is-one-quarter-desktop">
				<nav id="affixed-left-container" class="margin-top-sm-tablet position-fixed display-flex flex-direction-column" role="navigation" aria-label="Primary"></nav>
			</div>

			<!-- .primary-holder -->
			<section class="primary-holder column is-two-thirds-tablet is-three-quarters-desktop">
				<!--div.columns -->
				<div class="columns is-gapless-mobile has-large-gaps ">


					<div id="main-column" class="column  is-full is-8-desktop">

						<main id="main" role="main" data-bi-name="content" lang="en-us" dir="ltr">
							<!-- article-header -->
							<div id="article-header" class="background-color-body margin-top-sm-tablet margin-bottom-xs display-none-print">
								<div class="display-flex align-items-center ">
									<details id="article-header-breadcrumbs-overflow-popover" class="popover" data-for="article-header-breadcrumbs">
										<summary class="button button-clear button-primary button-sm" aria-label="All breadcrumbs">
											<span class="icon">
												<span class="docon docon-more"></span>
											</span>
										</summary>
										<div id="article-header-breadcrumbs-overflow" class="popover-content padding-none">

										</div>
									</details>
									<bread-crumbs id="article-header-breadcrumbs" class="overflow-hidden flex-grow-1 margin-right-sm margin-right-md-tablet margin-right-lg-desktop" id="page-breadcrumbs"></bread-crumbs>

									<div id="article-header-page-actions"  class="opacity-none margin-left-auto display-flex flex-wrap-no-wrap align-items-stretch">

										<a
											id="lang-link-tablet"
											class="button button-primary button-clear button-sm display-none display-inline-flex-tablet"
											title="Read in English" data-bi-name="language-toggle"
											data-read-in-link
											hidden>
											<span class="icon margin-none" aria-hidden="true" data-read-in-link-icon>
												<span class="docon docon-locale-globe"></span>
											</span>
											<span class="is-visually-hidden" data-read-in-link-text>Read in English</span>
										</a>

											<button
												type="button"
												class="collection button button-clear button-sm button-primary display-none display-inline-flex-tablet"
												data-list-type="collection"
												data-bi-name="collection"
												title="Add to collection">
												<span class="icon margin-none" aria-hidden="true">
													<span class="docon docon-circle-addition"></span>
												</span>
												<span class="collection-status is-visually-hidden">Save</span>
											</button>






											<a	data-contenteditbtn
												class="button button-clear button-sm text-decoration-none button-primary display-none display-inline-flex-tablet"
												aria-label="Edit"
												title="Edit This Document"
												data-bi-name="edit"

														href="https://github.com/MicrosoftDocs/sql-docs/blob/live/docs/t-sql/functions/key-id-transact-sql.md"
														data-original_content_git_url="https://github.com/MicrosoftDocs/sql-docs-pr/blob/live/docs/t-sql/functions/key-id-transact-sql.md"
														data-original_content_git_url_template="{repo}/blob/{branch}/docs/t-sql/functions/key-id-transact-sql.md"
														data-pr_repo=""
														data-pr_branch=""
											>
												<span class="icon margin-none" aria-hidden="true">
													<span class="docon docon-edit-outline"></span>
												</span>
											</a>




										<details class="popover popover-right" id="article-header-page-actions-overflow">
											<summary class="justify-content-flex-start button button-clear button-sm button-primary" aria-label="More actions">
												<span class="icon" aria-hidden="true">
													<span class="docon docon-more-vertical"></span>
												</span>
											</summary>
											<div class="popover-content padding-none">
													<button
														data-page-action-item="overflow-mobile"
														type="button"
														class="justify-content-flex-start button-block button-sm has-inner-focus button button-clear display-none-tablet"
														aria-label="Contents"
														data-bi-name="contents-expand"
														data-contents-button
														data-popover-close>
														<span class="icon">
															<span class="docon docon-editor-list-bullet" aria-hidden="true"></span>
														</span>
														<span class="contents-expand-title">Table of contents</span>
													</button>

												<a
													id="lang-link-overflow"
													class="justify-content-flex-start button-sm has-inner-focus button button-clear button-block display-none-tablet"
													title="Read in English" data-bi-name="language-toggle"
													data-page-action-item="overflow-mobile"
													data-check-hidden="true"
													data-read-in-link
													hidden
													>
													<span class="icon" aria-hidden="true" data-read-in-link-icon>
														<span class="docon docon-locale-globe"></span>
													</span>
													<span data-read-in-link-text>Read in English</span>
												</a>

													<button
														type="button"
														class="collection justify-content-flex-start button button-clear button-sm has-inner-focus button-block display-none-tablet"
														data-list-type="collection"
														data-bi-name="collection"
														title="Add to collection"
														data-page-action-item="overflow-mobile"
														data-check-hidden="true"
														data-popover-close>
														<span class="icon" aria-hidden="true">
															<span class="docon docon-circle-addition"></span>
														</span>
														<span class="collection-status">Save</span>
													</button>



													<a	data-contenteditbtn
														class="button button-clear button-block button-sm has-inner-focus justify-content-flex-start text-decoration-none display-none-tablet"
														aria-label="Edit"
														title="Edit This Document"
														data-bi-name="edit"

																href="https://github.com/MicrosoftDocs/sql-docs/blob/live/docs/t-sql/functions/key-id-transact-sql.md"
																data-original_content_git_url="https://github.com/MicrosoftDocs/sql-docs-pr/blob/live/docs/t-sql/functions/key-id-transact-sql.md"
																data-original_content_git_url_template="{repo}/blob/{branch}/docs/t-sql/functions/key-id-transact-sql.md"
																data-pr_repo=""
																data-pr_branch=""
													>
														<span class="icon" aria-hidden="true">
															<span class="docon docon-edit-outline"></span>
														</span>
														<span>Edit</span>
													</a>

													<button
														class="button button-block button-clear button-sm justify-content-flex-start has-inner-focus"
														title="Print"
														type="button"
														aria-label="Print"
														data-bi-name="print"
														data-page-action-item="overflow-all"
														data-popover-close
														data-print-page
														data-check-hidden="true"
														hidden>
														<span class="icon" aria-hidden="true">
															<span class="docon docon-print"></span>
														</span>
														<span>Print</span>
													</button>

													<div aria-hidden="true" class="margin-none display-none-tablet border-top" data-page-action-item="overflow-all"></div>


														<a class="button button-clear button-sm has-inner-focus button-block text-decoration-none justify-content-flex-start share-twitter" data-bi-name="twitter" data-page-action-item="overflow-all">
															<span class="icon" aria-hidden="true">
																<span class="docon docon-brand-twitter"></span>
															</span>
															<span>Twitter</span>
														</a>
														<a class="button button-clear button-sm has-inner-focus button-block text-decoration-none justify-content-flex-start share-linkedin" data-bi-name="linkedin" data-page-action-item="overflow-all">
															<span class="icon" aria-hidden="true">
																<span class="docon docon-brand-linkedin"></span>
															</span>
															<span>LinkedIn</span>
														</a>
														<a class="button button-clear button-sm button-block has-inner-focus text-decoration-none justify-content-flex-start share-facebook" data-bi-name="facebook" data-page-action-item="overflow-all">
															<span class="icon" aria-hidden="true">
																<span class="docon docon-brand-facebook"></span>
															</span>
															<span>Facebook</span>
														</a>
														<a class="button button-clear button-sm button-block has-inner-focus text-decoration-none justify-content-flex-start share-email" data-bi-name="email" data-page-action-item="overflow-all">
															<span class="icon" aria-hidden="true">
																<span class="docon docon-mail-message-fill"></span>
															</span>
															<span>Email</span>
														</a>

											</div>
										</details>

									</div>
								</div>
							</div>
							<!-- end article-header -->


							<div class="">
								<button type="button" class="border contents-button button button-clear button-sm is-hidden-tablet has-inner-focus" aria-label="Contents" data-bi-name="contents-expand" data-contents-button hidden>
									<span class="icon">
										<span class="docon docon-editor-list-bullet" aria-hidden="true"></span>
									</span>
									<span class="contents-expand-title">Table of contents</span>
								</button>
							</div>

							<!-- end mobile-contents button  -->

							<div class="content ">


								<h1 id="key_id-transact-sql">KEY_ID (Transact-SQL)</h1>


									<div class="display-flex justify-content-space-between align-items-center flex-wrap-wrap page-metadata-container">
										<div class="margin-right-xxs">
											<ul class="metadata page-metadata" data-bi-name="page info" lang="en-us" dir="ltr">
													<li>
Article													</li>
													<li class="visibility-hidden-visual-diff">
														<time class="is-invisible" data-article-date aria-label="Article review date" datetime="2022-11-18T23:19:00Z" data-article-date-source="git">11/18/2022</time>
													</li>
															<li class="readingTime">2 minutes to read</li>
														<li class="contributors-holder display-none-print">
															<button aria-label="View all contributors" class="contributors-button link-button" data-bi-name="contributors" title="View all contributors">
																	9 contributors
															</button>
														</li>
											</ul>
										</div>

<div id="user-feedback" class="margin-block-xxs display-none-print" data-hide-on-archived>
	<button
		id="user-feedback-button"
		class="button button-sm button-clear button-primary"
		type="button"
		data-bi-name="user-feedback-button"
		data-user-feedback-button
	>
		<span class="icon" aria-hidden="true">
			<span class="docon docon-like"></span>
		</span>
		<span>Feedback</span>
	</button>
</div>
									</div>

										<nav id="center-doc-outline" class="doc-outline is-hidden-desktop display-none-print" data-bi-name="intopic toc" role="navigation" aria-label="Article Outline">
											<h3>In this article</h3>
										</nav>

								<!-- <content> -->
									<p><strong>Applies to:</strong> <img src="../../includes/media/yes-icon.svg?view=sql-server-ver16" role="presentation" data-linktype="relative-path">
 SQL Server (all supported versions)  <img src="../../includes/media/yes-icon.svg?view=sql-server-ver16" role="presentation" data-linktype="relative-path">
 Azure SQL Database <img src="../../includes/media/yes-icon.svg?view=sql-server-ver16" role="presentation" data-linktype="relative-path">
 Azure SQL Managed Instance</p>
<p>Returns the ID of a symmetric key in the current database.</p>
<p><img src="../../database-engine/configure-windows/media/topic-link.gif?view=sql-server-ver16" alt="Topic link icon" title="Topic link icon" data-linktype="relative-path"> <a href="../language-elements/transact-sql-syntax-conventions-transact-sql?view=sql-server-ver16" data-linktype="relative-path">Transact-SQL Syntax Conventions</a></p>
<h2 id="syntax">Syntax</h2>
<pre><code class="lang-syntaxsql">Key_ID ( 'Key_Name' )
</code></pre>
<div class="NOTE">
<p>Note</p>
<p>To view Transact-SQL syntax for SQL Server 2014 and earlier, see <a href="../../sql-server/previous-versions-sql-server?view=sql-server-ver16#offline-documentation" data-linktype="relative-path">Previous versions documentation</a>.</p>
</div>
<h2 id="arguments">Arguments</h2>
<p><strong>'</strong> <em>Key_Name</em> <strong>'</strong><br>
The name of a symmetric key in the database.</p>
<h2 id="return-types">Return Types</h2>
<p><strong>int</strong></p>
<h2 id="remarks">Remarks</h2>
<p>The name of a temporary key must start with a number sign (#).</p>
<h2 id="permissions">Permissions</h2>
<p>Because temporary keys are only available in the session in which they are created, no permissions are required to access them. To access a key that is not temporary, the caller needs some permission on the key and must not have been denied VIEW permission on the key.</p>
<h2 id="examples">Examples</h2>
<h3 id="a-returning-the-id-of-a-symmetric-key">A. Returning the ID of a symmetric key</h3>
<p>The following example returns the ID of a key called <code>ABerglundKey1</code>.</p>
<pre><code class="lang-sql">SELECT KEY_ID('ABerglundKey1');
</code></pre>
<h3 id="b-returning-the-id-of-a-temporary-symmetric-key">B. Returning the ID of a temporary symmetric key</h3>
<p>The following example returns the ID of a temporary symmetric key. Note that <code>#</code> is prepended to the key name.</p>
<pre><code class="lang-sql">SELECT KEY_ID('#ABerglundKey2');
</code></pre>
<h2 id="see-also">See Also</h2>
<p><a href="key-guid-transact-sql?view=sql-server-ver16" data-linktype="relative-path">KEY_GUID (Transact-SQL)</a><br>
<a href="../statements/create-symmetric-key-transact-sql?view=sql-server-ver16" data-linktype="relative-path">CREATE SYMMETRIC KEY (Transact-SQL)</a><br>
<a href="../../relational-databases/system-catalog-views/sys-symmetric-keys-transact-sql?view=sql-server-ver16" data-linktype="relative-path">sys.symmetric_keys (Transact-SQL)</a><br>
<a href="../../relational-databases/system-catalog-views/sys-key-encryptions-transact-sql?view=sql-server-ver16" data-linktype="relative-path">sys.key_encryptions (Transact-SQL)</a><br>
<a href="../../relational-databases/security/encryption/encryption-hierarchy?view=sql-server-ver16" data-linktype="relative-path">Encryption Hierarchy</a></p>

							</div>

							<div id="assertive-live-region" role="alert" aria-live="assertive" class="visually-hidden" aria-relevant="additions" aria-atomic="true"></div>
							<div id="polite-live-region" role="status" aria-live="polite" class="visually-hidden" aria-relevant="additions" aria-atomic="true"></div>
							<!-- </content> -->

						</main>




						<!-- recommendations section -->
							<section id="recommendations-section" data-bi-name="recommendations" class="display-none-print"></section>
							<section id="right-rail-recommendations-mobile" data-bi-name="recommendations" class="display-none-desktop margin-top-xxs" hidden></section>
						<!-- end recommendations section -->

							<section id="right-rail-learning-resources-mobile" class="display-none-desktop margin-top-xxs" data-bi-name="learning-resources-card" hidden></section>
							<section id="right-rail-qna-mobile" class="display-none-desktop margin-top-xxs" data-bi-name="qna-link-card" hidden></section>

						<!-- feedback section -->
						<!-- end feedback section -->

						<!-- feedback report section -->
						<!-- end feedback report section -->

							<section id="right-rail-events-mobile" class="display-none-desktop margin-top-xs" data-bi-name="events-card" hidden></section>

						<div class="border-top is-visible-interactive has-default-focus margin-top-sm ">



	<footer id="footer-interactive" data-bi-name="footer" class="footer-layout">
	<div class="display-flex is-full-height padding-right-lg-desktop">
			<a
				data-mscc-ic="false"
				class="locale-selector-link button button-clear flex-shrink-0"
				href="#"
				data-bi-name="select-locale">
					<span class="icon" aria-hidden="true">
						<span class="docon docon-world"></span>
					</span>
					<span class="local-selector-link-text"></span></a>
		<div class="margin-inline-xs flex-shrink-0">
<div class="dropdown has-caret-up">
	<button class="dropdown-trigger button button-clear button-sm has-inner-focus theme-dropdown-trigger"
		aria-controls="theme-menu-interactive" aria-expanded="false" title="Theme" data-bi-name="theme">
		<span class="icon">
			<span class="docon docon-sun" aria-hidden="true"></span>
		</span>
		<span>Theme</span>
	</button>
	<div class="dropdown-menu" id="theme-menu-interactive" role="menu">
		<ul class="theme-selector padding-xxs" role="none">
			<li class="theme display-block" role="menuitem">
				<button class="button button-clear button-sm theme-control button-block justify-content-flex-start"
					data-theme-to="light">
					<span class="theme-light margin-right-xxs">
						<span
							class="theme-selector-icon css-variable-support border display-inline-block has-body-background"
							aria-hidden="true">
							<svg class="svg" xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 22 14">
								<rect width="22" height="14" class="has-fill-body-background" />
								<rect x="5" y="5" width="12" height="4" class="has-fill-secondary" />
								<rect x="5" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="8" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="11" y="2" width="3" height="1" class="has-fill-secondary" />
								<rect x="1" y="1" width="2" height="2" class="has-fill-secondary" />
								<rect x="5" y="10" width="7" height="2" rx="0.3" class="has-fill-primary" />
								<rect x="19" y="1" width="2" height="2" rx="1" class="has-fill-secondary" />
							</svg>
						</span>
					</span>
					<span>
Light					</span>
				</button>
			</li>
			<li class="theme display-block" role="menuitem">
				<button class="button button-clear button-sm theme-control button-block justify-content-flex-start"
					data-theme-to="dark">
					<span class="theme-dark margin-right-xxs">
						<span
							class="border theme-selector-icon css-variable-support display-inline-block has-body-background"
							aria-hidden="true">
							<svg class="svg" xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 22 14">
								<rect width="22" height="14" class="has-fill-body-background" />
								<rect x="5" y="5" width="12" height="4" class="has-fill-secondary" />
								<rect x="5" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="8" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="11" y="2" width="3" height="1" class="has-fill-secondary" />
								<rect x="1" y="1" width="2" height="2" class="has-fill-secondary" />
								<rect x="5" y="10" width="7" height="2" rx="0.3" class="has-fill-primary" />
								<rect x="19" y="1" width="2" height="2" rx="1" class="has-fill-secondary" />
							</svg>
						</span>
					</span>
					<span>
Dark					</span>
				</button>
			</li>
			<li class="theme display-block" role="menuitem">
				<button class="button button-clear button-sm theme-control button-block justify-content-flex-start"
					data-theme-to="high-contrast">
					<span class="theme-high-contrast margin-right-xxs">
						<span
							class="border theme-selector-icon css-variable-support display-inline-block has-body-background"
							aria-hidden="true">
							<svg class="svg" xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 22 14">
								<rect width="22" height="14" class="has-fill-body-background" />
								<rect x="5" y="5" width="12" height="4" class="has-fill-secondary" />
								<rect x="5" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="8" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="11" y="2" width="3" height="1" class="has-fill-secondary" />
								<rect x="1" y="1" width="2" height="2" class="has-fill-secondary" />
								<rect x="5" y="10" width="7" height="2" rx="0.3" class="has-fill-primary" />
								<rect x="19" y="1" width="2" height="2" rx="1" class="has-fill-secondary" />
							</svg>
						</span>
					</span>
					<span>
High contrast					</span>
				</button>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
	<ul class="links" data-bi-name="footerlinks">
		<li class="manage-cookies-holder" hidden></li>
				<li><a data-mscc-ic="false" href="/en-us/previous-versions/" data-bi-name="archivelink">Previous Versions</a></li>
				<li><a data-mscc-ic="false" href="https://techcommunity.microsoft.com/t5/microsoft-learn-blog/bg-p/MicrosoftLearnBlog" data-bi-name="bloglink">Blog</a></li>
				<li><a data-mscc-ic="false" href="/en-us/contribute/" data-bi-name="contributorGuide">Contribute</a></li>
					<li><a data-mscc-ic="false" href="https://go.microsoft.com/fwlink/?LinkId=521839" data-bi-name="privacy">Privacy</a></li>
				<li><a data-mscc-ic="false" href="/en-us/legal/termsofuse" data-bi-name="termsofuse">Terms of Use</a></li>
				<li><a data-mscc-ic="false" href="https://www.microsoft.com/en-us/legal/intellectualproperty/Trademarks/EN-US.aspx" data-bi-name="trademarks">Trademarks</a></li>
		<li>&copy; Microsoft 2022</li>
	</ul>
</footer>
						</div>

					</div>

						<div
							class="font-size-sm right-container column is-4-desktop display-none display-block-desktop"
							data-bi-name="pageactions"
							role="complementary"
							aria-label="Page Actions">

							<div id="affixed-right-container" class="margin-top-sm-tablet doc-outline is-vertically-scrollable" data-bi-name="right-column">
								<section id="right-rail-events" data-bi-name="events-card" class="margin-bottom-xxs" hidden></section>
								<section id="right-rail-recommendations" data-bi-name="recommendations" class="margin-bottom-xxs" hidden></section>
								<nav id="side-doc-outline" data-bi-name="intopic toc" role="navigation" aria-label="Article Outline">
									<h3>In this article</h3>
								</nav>
								<section id="right-rail-learning-resources" class="margin-top-xxs" data-bi-name="learning-resources-card" hidden></section>
								<section id="right-rail-qna" class="margin-top-xxs" data-bi-name="qna-link-card" hidden></section>
							</div>
						</div>

				</div>
				<!--end of div.columns -->

			</section>
			<!--end of .primary-holder -->

			<!-- interactive container -->
			<aside id="interactive-container" class="interactive-container is-visible-interactive column has-body-background-dark ">
			</aside>
			<!-- end of interactive container -->
		</div>

	</div>
	<!--end of .mainContainer -->

	<section class="border-top has-default-focus is-hidden-interactive margin-top-sm ">



	<footer id="footer" data-bi-name="footer" class="footer-layout uhf-container has-padding" role="contentinfo">
	<div class="display-flex is-full-height padding-right-lg-desktop">
			<a
				data-mscc-ic="false"
				class="locale-selector-link button button-clear flex-shrink-0"
				href="#"
				data-bi-name="select-locale">
					<span class="icon" aria-hidden="true">
						<span class="docon docon-world"></span>
					</span>
					<span class="local-selector-link-text"></span></a>
		<div class="margin-inline-xs flex-shrink-0">
<div class="dropdown has-caret-up">
	<button class="dropdown-trigger button button-clear button-sm has-inner-focus theme-dropdown-trigger"
		aria-controls="theme-menu" aria-expanded="false" title="Theme" data-bi-name="theme">
		<span class="icon">
			<span class="docon docon-sun" aria-hidden="true"></span>
		</span>
		<span>Theme</span>
	</button>
	<div class="dropdown-menu" id="theme-menu" role="menu">
		<ul class="theme-selector padding-xxs" role="none">
			<li class="theme display-block" role="menuitem">
				<button class="button button-clear button-sm theme-control button-block justify-content-flex-start"
					data-theme-to="light">
					<span class="theme-light margin-right-xxs">
						<span
							class="theme-selector-icon css-variable-support border display-inline-block has-body-background"
							aria-hidden="true">
							<svg class="svg" xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 22 14">
								<rect width="22" height="14" class="has-fill-body-background" />
								<rect x="5" y="5" width="12" height="4" class="has-fill-secondary" />
								<rect x="5" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="8" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="11" y="2" width="3" height="1" class="has-fill-secondary" />
								<rect x="1" y="1" width="2" height="2" class="has-fill-secondary" />
								<rect x="5" y="10" width="7" height="2" rx="0.3" class="has-fill-primary" />
								<rect x="19" y="1" width="2" height="2" rx="1" class="has-fill-secondary" />
							</svg>
						</span>
					</span>
					<span>
Light					</span>
				</button>
			</li>
			<li class="theme display-block" role="menuitem">
				<button class="button button-clear button-sm theme-control button-block justify-content-flex-start"
					data-theme-to="dark">
					<span class="theme-dark margin-right-xxs">
						<span
							class="border theme-selector-icon css-variable-support display-inline-block has-body-background"
							aria-hidden="true">
							<svg class="svg" xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 22 14">
								<rect width="22" height="14" class="has-fill-body-background" />
								<rect x="5" y="5" width="12" height="4" class="has-fill-secondary" />
								<rect x="5" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="8" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="11" y="2" width="3" height="1" class="has-fill-secondary" />
								<rect x="1" y="1" width="2" height="2" class="has-fill-secondary" />
								<rect x="5" y="10" width="7" height="2" rx="0.3" class="has-fill-primary" />
								<rect x="19" y="1" width="2" height="2" rx="1" class="has-fill-secondary" />
							</svg>
						</span>
					</span>
					<span>
Dark					</span>
				</button>
			</li>
			<li class="theme display-block" role="menuitem">
				<button class="button button-clear button-sm theme-control button-block justify-content-flex-start"
					data-theme-to="high-contrast">
					<span class="theme-high-contrast margin-right-xxs">
						<span
							class="border theme-selector-icon css-variable-support display-inline-block has-body-background"
							aria-hidden="true">
							<svg class="svg" xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 22 14">
								<rect width="22" height="14" class="has-fill-body-background" />
								<rect x="5" y="5" width="12" height="4" class="has-fill-secondary" />
								<rect x="5" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="8" y="2" width="2" height="1" class="has-fill-secondary" />
								<rect x="11" y="2" width="3" height="1" class="has-fill-secondary" />
								<rect x="1" y="1" width="2" height="2" class="has-fill-secondary" />
								<rect x="5" y="10" width="7" height="2" rx="0.3" class="has-fill-primary" />
								<rect x="19" y="1" width="2" height="2" rx="1" class="has-fill-secondary" />
							</svg>
						</span>
					</span>
					<span>
High contrast					</span>
				</button>
			</li>
		</ul>
	</div>
</div>
		</div>
	</div>
	<ul class="links" data-bi-name="footerlinks">
		<li class="manage-cookies-holder" hidden></li>
				<li><a data-mscc-ic="false" href="/en-us/previous-versions/" data-bi-name="archivelink">Previous Versions</a></li>
				<li><a data-mscc-ic="false" href="https://techcommunity.microsoft.com/t5/microsoft-learn-blog/bg-p/MicrosoftLearnBlog" data-bi-name="bloglink">Blog</a></li>
				<li><a data-mscc-ic="false" href="/en-us/contribute/" data-bi-name="contributorGuide">Contribute</a></li>
					<li><a data-mscc-ic="false" href="https://go.microsoft.com/fwlink/?LinkId=521839" data-bi-name="privacy">Privacy</a></li>
				<li><a data-mscc-ic="false" href="/en-us/legal/termsofuse" data-bi-name="termsofuse">Terms of Use</a></li>
				<li><a data-mscc-ic="false" href="https://www.microsoft.com/en-us/legal/intellectualproperty/Trademarks/EN-US.aspx" data-bi-name="trademarks">Trademarks</a></li>
		<li>&copy; Microsoft 2022</li>
	</ul>
</footer>
	</section>

	<div id="action-panel" role="region" aria-label="Action Panel" class="action-panel has-default-focus" tabindex="-1"></div>
</body>
</html>
